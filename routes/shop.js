const express = require('express');
const router = express.Router();
const path = require('path');
const adminData = require('./admin');


router.get('/',(req,res,next)=>{
    console.log(adminData.products);
    const products = adminData.products;
    res.render('shop',{prods:products,title:"Shop Page"});
    // res.sendFile(path.join(__dirname,'../','views','shop.pug'))
});


module.exports = router;