// const fs = require("fs");

// fs.writeFileSync("hello.txt","Hello from Nodejs");
 

const Person = {
    fname:"Max",
    age:18,
    
    greet:()=>{
        return "hello";
    }
}
// console.log(Person.greet());

//destructuring object
const Destruct = ({ fname}) => {
    console.log(fname);
}
// Destruct(Person);
const {fname,age} = Person;
console.log(fname,age);
// Destructuring Hobbies
const hobbies = ["Programming","Fitness","Fun"];


const [hobby0,hobby1] = hobbies;
console.log(hobby0,hobby1);
// for(let hobby of hobbies ){
//     console.log(hobby);
// }

// const CopiedArray = hobbies.slice();

//spread operator (copying data of object or array)
const CopiedArray = [...hobbies];
// console.log(CopiedArray);

// Rest Operator (merging data into array using function args)
const toArray = (...args) =>{
    return args;
}
// console.log(toArray(1,2,3,4,5,6));

// console.log(hobbies.map(hobby => "hobby: "+hobby));
