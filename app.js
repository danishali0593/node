const http = require('http');
// const routes = require('./routes');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const adminData = require('./routes/admin');
const shopRouter = require('./routes/shop');
const path = require('path');

app.set('view engine','pug');
app.set('views','views');
 
app.use(bodyParser.urlencoded({extended:false}));

app.use(express.static(path.join(__dirname,'public')));

// app.use((req,res,next)=>{
//     console.log("In Middleware");
//     next();
// });

app.use('/admin',adminData.routes);
app.use(shopRouter);

app.use((req,res,next)=>{
    res.status(404).sendFile(path.join(__dirname,'views','404.html'));
});

// app.use((req,res,next)=>{
//     console.log("In another Middleware");
//     res.send("<h1>Hello Reader</h1>");
// });

// const server = http.createServer(app);
// server.listen(3000);

app.listen(3000);