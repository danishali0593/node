const fs = require('fs');

const requestHandler = (req,res)=>{
    const url = req.url;
    const method = req.method;
    if(url=== '/'){
        res.setHeader("Content-Type","text/html");
        res.write('<html>');
        res.write('<head><title>Node Js</title>');
        res.write('<body>');
        res.write('<h1>Welcome to Node js Server</h1>');
        res.write('<form action="/message" method="Post"><input type="text" name="message"/><button type="submit"> submit</button></form>')
        res.write('</body>');
        res.write('</html>');
        res.end();
    }
    if(url ==='/message' && method ==='POST'){
        const body = [];
        req.on('data',(chunk)=>{
            body.push(chunk);
        });
        req.on('end',()=>{
            const bodyParsed = Buffer.concat(body).toString();
            // console.log(bodyParsed);
            const message = bodyParsed.split('=')[1];

            fs.writeFile('message.txt',message, err =>{
            res.statusCode = 302;
            res.setHeader('Location','/');
            return res.end();

            });            
        });       
    }
}

module.exports = requestHandler;